from dataclasses import dataclass, field
from dataclasses_json import dataclass_json
from typing import List

from flask import Flask, request

import json
from abc import abstractclassmethod, abstractmethod

app = Flask(__name__, static_url_path = "")

@dataclass_json
@dataclass
class File:
    name: str
    text: str

@dataclass_json
@dataclass
class Test:
    id: str
    input: str
    output: str

@dataclass_json
@dataclass
class Request:
    files: List[File] = field(default_factory=list)
    tests: List[Test] = field(default_factory=list)

@app.route('/execute', methods = ['POST'])
def execute():
    return '200: OK'

if __name__ == '__main__':
    app.run(debug = True)
