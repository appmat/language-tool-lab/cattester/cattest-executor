FROM python:3.7

WORKDIR /srv

WORKDIR /code
COPY ./code .

CMD [ "python", "./runner.py" ]